const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Buiding = require('./models/Building')

async function main () {
  const newInformaticsBuilding = await Buiding.findById('621a28d0e02f89130e377a08')
  const room = await Room.findById('621a28d0e02f89130e377a0d')
  const informaticsBuilding = await Buiding.findById(room.building)
  console.log(newInformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)
  room.building = newInformaticsBuilding
  newInformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)
  room.save()
  newInformaticsBuilding.save()
  informaticsBuilding.save()
}

main().then(function () {
  console.log('Finish')
})
